-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Feb 2023 pada 11.25
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `is_active`) VALUES
(1, 'rey', 'raihan@gmail.com', '$2y$10$3QjeU0gnFfIMDLVFhZfvreU5ULkEH91/iFSkakiGI/W4uztt2y.P6', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(100) NOT NULL,
  `brand_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_title`) VALUES
(1, 'Demon Slayer'),
(2, 'Naruto Shippuden'),
(3, 'Attack On Titan'),
(4, 'Dragon Ball'),
(5, 'My Hero Academia'),
(6, 'Anime');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE `cart` (
  `id` int(10) NOT NULL,
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(250) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cart`
--

INSERT INTO `cart` (`id`, `p_id`, `ip_add`, `user_id`, `qty`) VALUES
(3, 5, '::1', 1, 1),
(4, 4, '::1', 1, 1),
(8, 6, '::1', 2, 1),
(10, 8, '::1', 2, 1),
(11, 24, '::1', 2, 1),
(17, 21, '::1', 3, 1),
(18, 19, '::1', 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(100) NOT NULL,
  `cat_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(2, 'Hoodie'),
(3, 'Kaos'),
(4, 'Figure'),
(5, 'DVD/PS/PSP'),
(6, 'Manga'),
(13, 'Anime');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `trx_id` varchar(255) NOT NULL,
  `p_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `product_id` int(100) NOT NULL,
  `product_cat` int(11) NOT NULL,
  `product_brand` int(100) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_keywords` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`product_id`, `product_cat`, `product_brand`, `product_title`, `product_price`, `product_qty`, `product_desc`, `product_image`, `product_keywords`) VALUES
(6, 4, 1, 'Kimetsu no Yaiba - Hashibira Inosuke - Figuarts ZERO - Beast Breathing (Bandai Spirits)', 1222, 100, 'Kimetsu no Yaiba - Hashibira Inosuke - Figuarts ZERO - Beast Breathing (Bandai Spirits)', '1675488223_inosuke.jpg', 'demon, figure, inosuke'),
(7, 4, 1, 'Kimetsu no Yaiba - Kamado Tanjirou - Figuarts ZERO - Total Concentration (Bandai Spirits)', 1000, 100, 'Kimetsu no Yaiba - Kamado Tanjirou - Figuarts ZERO - Total Concentration (Bandai Spirits)', '1675488459_tanjiro.jpg', 'demon, figure, tanjiro'),
(8, 4, 1, 'Kimetsu no Yaiba - Kochou Shinobu - ARTFX J - 1/8 (Kotobukiya) Kochou Shinobu', 1455, 100, 'Kimetsu no Yaiba - Kochou Shinobu - ARTFX J - 1/8 (Kotobukiya)', '1675488621_shinobu.jpg', 'demon, figure, shinobu'),
(9, 4, 1, 'Kimetsu no Yaiba - Agatsuma Zenitsu - ARTFX J - 1/8 (Kotobukiya)(Good Smile Company)', 700, 100, 'Kimetsu no Yaiba - Agatsuma Zenitsu - ARTFX J - 1/8 (Kotobukiya)', '1675488722_zenitsu.jpg', 'demon, figure, zenitsu'),
(10, 4, 1, 'Kimetsu no Yaiba - Kamado Nezuko - ARTFX J - 1/8 (Kotobukiya)(Good Smile Company)', 500, 100, 'Kimetsu no Yaiba - Kamado Nezuko - ARTFX J - 1/8 (Kotobukiya)', '1675488825_nezuko.jpg', 'demon, figure, nezuko'),
(11, 4, 1, 'Kimetsu no Yaiba - Kamado Tanjirou - Nendoroid - Nendoroid Swacchao (Good Smile Company)', 422, 100, 'Kimetsu no Yaiba - Kamado Tanjirou - Nendoroid - Nendoroid Swacchao (Good Smile Company)', '1675489075_tanjiro1.jpg', 'demon, figure, tanjiro'),
(12, 4, 1, 'Kimetsu no Yaiba - Kamado Nezuko - Nendoroid #1194 - December 2021 Re-release (Good Smile Company)', 500, 100, 'Kimetsu no Yaiba - Kamado Nezuko - Nendoroid #1194 - December 2021 Re-release (Good Smile Company)', '1675490624_nezuko1.jpg', 'demon, figure, nezuko'),
(14, 4, 1, 'Kimetsu no Yaiba - Kamado Tanjirou - ARTFX J - 1/8 (Kotobukiya) Kamado Tanjirou', 1444, 100, 'Kimetsu no Yaiba - Kamado Tanjirou - ARTFX J - 1/8 (Kotobukiya) Kamado Tanjirou', '1675491004_tanjiro2.jpg', 'demon, figure, tanjiro'),
(15, 4, 2, 'Naruto - Uchiha Sasuke - Door Painting Collection Figure - 1/7 (Plex)Collection Figure', 1555, 100, 'Naruto - Uchiha Sasuke - Door Painting Collection Figure - 1/7 (Plex)', '1675491187_sasuke.jpg', 'sasuke, figure, uchiha'),
(16, 4, 2, 'Naruto - Temari - Naruto Gals (MegaHouse)Anime Girl Collection Figure Temari', 1222, 100, 'Naruto - Temari - Naruto Gals (MegaHouse)Anime Girl Collection Figure', '1675491543_temari.jpg', 'temari, figure'),
(17, 4, 2, 'Naruto Shippuuden - Uchiha Itachii - S.H.Figuarts - The Famed Sharingan Hero (Bandai Spirits)', 1555, 100, 'Naruto Shippuuden - Uchiha Itachi - S.H.Figuarts - The Famed Sharingan Hero (Bandai Spirits)', '1675492060_itachi.jpg', 'itachi, figure, uchiha'),
(18, 4, 2, 'Naruto Shippuuden - Hatake Kakashi - G.E.M. - Ninkai Taisen ver. (MegaHouse)Ninkai Taisen', 1765, 100, 'Naruto Shippuuden - Hatake Kakashi - G.E.M. - Ninkai Taisen ver. (MegaHouse)', '1675491804_kakashi1.jpg', 'kakashi, figure'),
(19, 4, 2, 'Naruto Shippuuden - Hyuuga Hinata - Naruto Gals - 1/8 - Ver.2 (MegaHouse)anime girl', 1334, 100, 'Naruto Shippuuden - Hyuuga Hinata - Naruto Gals - 1/8 - Ver.2 (MegaHouse)', '1675491910_hinata.jpg', 'hinata, figure'),
(20, 4, 2, 'Naruto Shippuuden - Uzumaki Naruto - G.E.M. - 1/8 - Rikudou Sennin Mode (MegaHouse)', 1655, 100, 'Naruto Shippuuden - Uzumaki Naruto - G.E.M. - 1/8 - Rikudou Sennin Mode (MegaHouse)', '1675492189_naruto.jpg', 'naruto, figure'),
(21, 4, 2, 'Naruto Shippuuden - Uchiha Sasuke - S.H.Figuarts - He who bears all Hatred (Bandai Spirits)', 1666, 100, 'Naruto Shippuuden - Uchiha Sasuke - S.H.Figuarts - He who bears all Hatred (Bandai Spirits)', '1675492263_sasuke1.jpg', 'sasuke, figure, uchiha'),
(22, 4, 2, 'Naruto Shippuuden - Uzumaki Naruto - S.H.Figuarts - The Jinchuuriki entrusted with Hope (Bandai Spirits)', 1555, 100, 'Naruto Shippuuden - Uzumaki Naruto - S.H.Figuarts - The Jinchuuriki entrusted with Hope (Bandai Spirits)', '1675492415_naruto1.jpg', 'naruto, figure'),
(23, 4, 3, 'Shingeki no Kyojin The Final Season - Eren Yeager - Pop Up Parade (Good Smile Company)', 1333, 100, 'Shingeki no Kyojin The Final Season - Eren Yeager - Pop Up Parade (Good Smile Company)', '1675492736_eren.jpg', 'eren, aot, figure'),
(24, 4, 3, 'Shingeki no Kyojin - Levi - BRAVE-ACT - 1/8 (Sentinel) Action Figure Attack on Titan Levi', 1222, 100, 'Shingeki no Kyojin - Levi - BRAVE-ACT - 1/8 (Sentinel) Action Figure AOTlevi', '1675497202_levi.jpg', 'levi, aot, ackerman, figure'),
(25, 4, 3, 'Shingeki no Kyojin - Mikasa Ackerman - ARTFX J - 1/8 (Kotobukiya)Attack on Titan ', 1999, 100, 'Shingeki no Kyojin - Mikasa Ackerman - ARTFX J - 1/8 (Kotobukiya)', '1675497699_mikasa.jpg', 'mikasa, aot, ackerman, figure'),
(26, 4, 3, 'Shingeki no Kyojin - Mikasa Ackerman - Figma #203 (Max Factory)Attack On Titan', 1666, 100, 'Shingeki no Kyojin - Mikasa Ackerman - Figma #203 (Max Factory)', '1675498219_mikasa2.jpg', 'mikasa, aot, ackerman ,figure'),
(27, 4, 3, 'Shingeki no Kyojin - Eren Yeager - Figma #207 (Max Factory)Attack On Titan', 1443, 100, 'Shingeki no Kyojin - Eren Yeager - Figma #207 (Max Factory)', '1675498121_eren1.jpg', 'eren, aot, figure'),
(28, 4, 3, 'Shingeki no Kyojin - Eren Rogue Titan - Pop Up Parade (Good Smile Company)  AOT ', 1760, 100, 'Shingeki no Kyojin - Rogue Titan - Pop Up Parade (Good Smile Company)', '1675498375_titan.jpg', 'eren, aot, figure, titan'),
(29, 4, 3, 'Shingeki no Kyojin - Levi - Figma #EX-020 - Cleaning ver.figma Attack On Titan F', 2445, 100, 'Shingeki no Kyojin - Levi - Figma #EX-020 - Cleaning ver.figma Attack On Titan F', '1675498782_levi1.jpg', 'levi, aot, ackerman, figure'),
(30, 4, 3, 'Shingeki no Kyojin The Final Season - Reiner Braun - Pop Up Parade (Good Smile Company)', 1700, 100, 'Shingeki no Kyojin The Final Season - Reiner Braun - Pop Up Parade (Good Smile Company)', '1675499020_reinar.jpg', 'reinar, aot, figure'),
(32, 3, 6, 'Kaos Anime Baju Naruto SASUKE ITACHI UCHIHA Pakaian Pria ', 12, 100, 'Kaos Anime Baju Naruto SASUKE ITACHI UCHIHA Pakaian Pria |', '1675499566_kaos.jpg', 'kaos, itachi, uchiha'),
(33, 3, 6, 'Jual BAJU KAOS PRIA ANIME AKATSUKI NARUTO GYM FITNES', 14, 100, 'Jual BAJU KAOS PRIA ANIME AKATSUKI NARUTO GYM FITNES RENANG LARI', '1675499713_BAJU_KAOS_PRIA_ANIME_AKATSUKI_NARUTO_GYM_FITNES_RENANG_LARI_.jpg', 'kaos, itachi, akatsuki'),
(34, 3, 6, 'Manjiro Sano Tokyo Revengers T-Shirt Anime T-Shirt - Maukaos | Shopee ', 10, 100, 'Manjiro Sano Tokyo Revengers T-Shirt Anime T-Shirt - Maukaos | Shopee ...', '1675499949_23a391512057af3ad89c7332acd2e3ef.jpg', 'kaos, mikey, anime'),
(35, 3, 6, 'Jual Kaos Baju Anime HUNTER X HUNTER KILLUA ZOLDYCK V6 Kaos', 27, 100, 'Jual Kaos Baju Anime HUNTER X HUNTER KILLUA ZOLDYCK V6 Kaos', '1675500208_33f33737-b500-4cbf-a6b4-c387ed6eb13a.png', 'kaos, killua, hxh'),
(37, 2, 6, 'Amazon.com: Dragonball Anime T-Shirt Unisex Short-Sleeved', 29, 100, 'Amazon.com Anime T-Shirt Unisex Short-Sleeved 3D ...', '1675500633_OIP (1).jpg', 'hoodie, goku, dragonball'),
(39, 2, 6, 'Anime Kenshin Adult Hoodie – Addictive Kaos shopaddictivekaos.com', 22, 100, 'Anime Kenshin Adult Hoodie – Addictive Kaos\r\nshopaddictivekaos.com', '1675500804_spod-10505.jpg', 'hoodie, anime'),
(41, 2, 6, 'Anime Tokyo Ghoul Kaneki - Camisetas Anime | Presentes ...', 30, 100, 'Anime Tokyo Ghoul Kaneki - Camisetas Anime | Presentes ...\r\n', '1675500928_kaneki.jpg', 'kaos, kaneki');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_info`
--

INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address1`, `address2`) VALUES
(1, 'ara', 'ara', 'araara@gmail.com', '0862df1ba6a17272b11b2a548ec6e8f7', '0202020202', 'araarastreet', 'araarastree'),
(2, 'Pembeli', 'san', 'pembeli@gmail.com', 'f64f963c2f43c9dd68ecb4fa529a1511', '1234567890', 'tokyo', 'yokohama'),
(3, 'raihan', 'kun', 'raihankun@gmail.com', '156fb0d6fb22b2653fa944a5080b56e5', '0987654321', 'awe', 'awe');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indeks untuk tabel `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indeks untuk tabel `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `fk_product_cat` (`product_cat`),
  ADD KEY `fk_product_brand` (`product_brand`);

--
-- Indeks untuk tabel `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_product_brand` FOREIGN KEY (`product_brand`) REFERENCES `brands` (`brand_id`),
  ADD CONSTRAINT `fk_product_cat` FOREIGN KEY (`product_cat`) REFERENCES `categories` (`cat_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
